# uas-pwm

### Project Setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```


API :
install json-server https://github.com/typicode/json-server

jalankan via command : 
json-server -w db.json

### Anggota Kelompok
- Amadea Hastalina 215411137
- Arief Dolants 205411192
- Aurelsa Puspitasari 215411153
- Tediyan Rahmat W 195411119
- Triyan Agung L 215411151
